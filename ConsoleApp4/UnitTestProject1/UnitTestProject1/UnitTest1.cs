﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp4;

namespace ConsoleApp4.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CalledOperationWithTypeA()
        {
            Class1 class1 = new Class1();
            string result = class1.Operation1("a");

            Assert.AreEqual("aa", result);
        }

        //[TestMethod]
        //public void CalledOperationWithTypeB()
        //{
        //    Class1 class1 = new Class1();
        //    string result = class1.Operation1("b");

        //    Assert.AreEqual("bb", result);
        //}

        //[TestMethod]
        //public void CalledOperationWithTypeBB()
        //{
        //    Class1 class1 = new Class1();
        //    string result = class1.Operation1("bb");

        //    Assert.AreEqual("bb", result);
        //}
    }
}