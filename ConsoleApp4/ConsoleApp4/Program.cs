﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4.MyProgram
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var str = "a";
                new Class1().Operation1("a");
                Console.ReadLine();

                new Class1().Operation1("b");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}